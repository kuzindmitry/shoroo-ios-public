//
//  ProductModel.swift
//  shoroo
//
//  Created by kuzindmitry on 02.04.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import Foundation

struct ProductModel: Decodable {
    let id: Int
    let title: String
    let state: String
    let price: Int
    let image: String
}
