//
//  ProductResult.swift
//  shoroo
//
//  Created by kuzindmitry on 02.04.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import Foundation

struct ProductResult: Decodable {
    let code: Int
    let status: String
    let results: [ProductModel]?
}
