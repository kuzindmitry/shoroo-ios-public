//
//  API.swift
//  shoroo
//
//  Created by kuzindmitry on 02.01.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import Foundation

struct API {
    static let host: APIPath = "https://shoroo.ru/api"
}

extension APIPath {
    
    var auth: APIPath {
        return self + "/auth"
    }
    
    var signin: APIPath {
        return self + "/signin"
    }
    
    var signup: APIPath {
        return self + "/signup"
    }
    
    var social: APIPath {
        return self + "/social"
    }
    
    func user(id: Int = 0) -> APIPath {
        var string = "/user"
        if id == 0 {
            string += "/self"
        } else {
            string += "/\(id)"
        }
        return self + string
    }
    
    var resetPassword: APIPath {
        return self + "/reset/password"
    }
    
    var token: APIPath {
        return self + "/token"
    }
    
    var refresh: APIPath {
        return self + "/refresh"
    }
    
    func activeProduct(offset: Int = 0, token: Token) -> APIPath {
        return self + "/product/active/" + token.access + "?offset=\(offset)"
    }
    
}
