//
//  TokenModel.swift
//  shoroo
//
//  Created by kuzindmitry on 02.04.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import Foundation

struct TokenModel: Decodable {
    let user_id: Int
    let access_token: String
    let expires: Int64
    let refresh_token: String
}
