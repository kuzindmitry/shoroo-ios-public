//
//  UserModel.swift
//  shoroo
//
//  Created by kuzindmitry on 02.04.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import Foundation

struct UserModel: Decodable {
    let id: Int
    let email: String
    let phone: String
    let first_name: String
    let last_name: String
    let showroom: Int
}
