//
//  ProductParser.swift
//  shoroo
//
//  Created by kuzindmitry on 02.04.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import Foundation

class ProductParser {
    
    let product: Product
    
    init(model: ProductModel) {
        product = Product()
        product.id = model.id
        product.state = model.state
        product.title = model.title
        product.price = model.price
        product.image = model.image
        try? Database.shared.realm.write {
            Database.shared.realm.add(product, update: true)
        }
    }
    
}
