//
//  TokenParser.swift
//  shoroo
//
//  Created by kuzindmitry on 02.04.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import Foundation

class TokenParser {
    
    let token: Token
    
    init(model: TokenModel) {
        token = Token()
        token.access = model.access_token
        token.refresh = model.refresh_token
        token.expires = Date(timeIntervalSince1970: Double(model.expires))
        token.userId = model.user_id
        try? Database.shared.realm.write {
            Database.shared.realm.add(token, update: true)
        }
    }
    
}
