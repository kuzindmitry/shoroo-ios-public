//
//  UserParser.swift
//  shoroo
//
//  Created by kuzindmitry on 01.01.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import Foundation

class UserParser {
    
    let user: User
    
    init(model: UserModel) {
        user = User()
        user.id = model.id
        user.email = model.email
        user.firstName = model.first_name
        user.lastName = model.last_name
        user.showroom = model.showroom
        user.phone = model.phone
        try? Database.shared.realm.write {
            Database.shared.realm.add(user, update: true)
        }
    }
    
}


