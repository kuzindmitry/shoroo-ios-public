//
//  User.swift
//  shoroo
//
//  Created by kuzindmitry on 01.01.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var email: String = ""
    @objc dynamic var phone: String = ""
    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var showroom: Int = 0
    var isShowroom: Bool {
        return showroom != 0
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
