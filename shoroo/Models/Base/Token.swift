//
//  Token.swift
//  shoroo
//
//  Created by kuzindmitry on 02.04.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import Foundation
import RealmSwift

class Token: Object {
    
    @objc dynamic var access: String = ""
    @objc dynamic var refresh: String = ""
    @objc dynamic var expires: Date = Date()
    @objc dynamic var userId: Int = 0
    
    override static func primaryKey() -> String? {
        return "userId"
    }
    
}
