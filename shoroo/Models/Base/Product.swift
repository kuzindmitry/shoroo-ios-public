//
//  Product.swift
//  shoroo
//
//  Created by kuzindmitry on 02.04.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import Foundation
import RealmSwift

class Product: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var title: String = ""
    @objc dynamic var state: String = ""
    @objc dynamic var price: Int = 0
    @objc dynamic var image: String = ""
    
    var imageUrl: URL? {
        return URL(string: image)
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
