//
//  Database.swift
//  shoroo
//
//  Created by kuzindmitry on 01.01.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import Foundation
import RealmSwift

class Database {
    static let shared: Database = Database()
    let realm: Realm
    
    private init() {
        realm = try! Realm()
    }
    
    func removeBase() {
        try? realm.write {
            realm.deleteAll()
        }
    }
}

class DataManager {
    
    static let shared: DataManager = DataManager()
    
    func currentToken() -> Token? {
        return (Database.shared.realm.objects(Token.self).filter{$0.expires > Date()}).first
    }
    
    func lastToken() -> Token? {
        return (Database.shared.realm.objects(Token.self).sorted{$0.expires > $1.expires}).first
    }
    
    func tokensCount() -> Int {
        return Database.shared.realm.objects(Token.self).count
    }
    
    func currentUser() -> User? {
        guard let token = currentToken() else { return nil }
        return Database.shared.realm.objects(User.self).filter{$0.id == token.userId}.first
    }
    
}
