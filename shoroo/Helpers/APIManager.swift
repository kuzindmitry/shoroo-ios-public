//
//  APIManager.swift
//  shoroo
//
//  Created by kuzindmitry on 02.01.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import UIKit

enum SocialType: String {
    case vk = "vk"
}

class APIManager {
    
    static let shared: APIManager = APIManager()
    
    func token(_ completion: @escaping (Token?) -> Void) {
        if let token = DataManager.shared.currentToken() {
            completion(token)
            return
        } else {
            if let lastToken = DataManager.shared.lastToken() {
                refreshToken(lastToken.refresh, { (newToken) in
                    completion(newToken)
                })
            } else {
                completion(nil)
            }
        }
    }
    
    var isRefreshing: Bool = false
    func refreshToken(_ refresh: String, _ completion: @escaping (Token?) -> Void) {
        if !isRefreshing {
            isRefreshing = true
            print("refresh token")
            DKNetwork.shared.url(API.host.auth.token.refresh).parameters(["refresh_token": refresh]).sendAuth({ (tokenModel) in
                guard let tokenModel = tokenModel else {
                    self.isRefreshing = false
                    completion(nil)
                    return
                }
                let parser = TokenParser(model: tokenModel)
                self.isRefreshing = false
                completion(parser.token)
            })
        }
    }
    
    func user(_ userId: Int = 0, _ completion: @escaping (User?) -> Void) {
        token { (token) in
            guard let token = token else {
                self.invalidToken()
                return
            }
            DKNetwork.shared.url(API.host.user(id: userId)).method(.post).parameters(["access_token": token.access]).sendUser({ (result) in
                guard let result = result else { completion(nil); return }
                if result.code == 401 {
                    self.invalidToken()
                    return
                }
                if let userModel = result.user, result.code == 200 {
                    let parser = UserParser(model: userModel)
                    completion(parser.user)
                } else {
                    completion(nil)
                }
            })
        }
    }
    
    func signin(email: String, password: String, _ completion: @escaping (Token?) -> Void) {
        auth(API.host.auth.signin, ["email": email, "password": password, "platform": "ios"], completion)
    }
    
    func signup(email: String, password: String, firstName: String, lastName: String, _ completion: @escaping (Token?) -> Void) {
        auth(API.host.auth.signup, ["email": email, "password": password, "first_name": firstName, "last_name": lastName, "platform": "ios"], completion)
    }
    
    func socialAuth(type: SocialType, email: String, token: String, userId: String, _ completion: @escaping (Token?) -> Void) {
        auth(API.host.auth.social, ["type": type.rawValue, "access_token": token, "email": email, "user_id": userId, "platform": "ios"], completion)
    }
    
    func resetPassword(email: String, _ completion: @escaping (Bool) -> Void) {
        DKNetwork.shared.url(API.host.auth.resetPassword).method(.post).parameters(["email": email]).sendUser { (result) in
            completion(result?.code == 200)
        }
    }
    
    private func auth(_ url: APIPath, _ credentials: [String: String], _ completion: @escaping (Token?) -> Void) {
        
        print(credentials)
        DKNetwork.shared.url(url).method(.post).parameters(credentials).sendAuth { (tokenModel) in
            guard let tokenModel = tokenModel else { completion(nil); return }
            let parser = TokenParser(model: tokenModel)
            completion(parser.token)
        }
    }
    
    func products(offset: Int, _ completion: @escaping ([Product])->Void) {
        token { (token) in
            guard let token = token else {
                self.invalidToken()
                return
            }
            DKNetwork.shared.url(API.host.activeProduct(offset: offset, token: token)).method(.get).sendProduct({ (models) in
                var results = [Product]()
                for model in models {
                    let parser = ProductParser(model: model)
                    results.append(parser.product)
                }
                completion(results)
            })
        }
    }
    
    private func invalidToken() {
        print("invalid token")
        DispatchQueue.main.async {
            let tokens =
                Database.shared.realm.objects(Token.self)
            let users = Database.shared.realm.objects(User.self)
            try? Database.shared.realm.write {
                Database.shared.realm.delete(tokens)
                Database.shared.realm.delete(users)
            }
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.window = UIWindow(frame: UIScreen.main.bounds)
            appDelegate?.window?.rootViewController = R.storyboard.authorization.instantiateInitialViewController()!
            appDelegate?.window?.makeKeyAndVisible()
        }
    }
    
}
