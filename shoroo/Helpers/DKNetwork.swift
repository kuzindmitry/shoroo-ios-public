//
//  DKNetwork.swift
//  pchat
//
//  Created by kuzindmitry on 04.02.2018.
//  Copyright © 2018 TASS. All rights reserved.
//

import Foundation
import UIKit

typealias APIPath = String

enum DKNetworkRequestMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

class DKNetwork {
    
    private var method: DKNetworkRequestMethod = .get
    private var request: URLRequest!
    
    static let shared: DKNetwork = DKNetwork()
    
    func url(_ urlPath: APIPath) -> Self {
        guard let url = URL(string: urlPath) else { return self }
        request = URLRequest(url: url)
        return self
    }

    func method(_ method: DKNetworkRequestMethod) -> Self {
        guard request != nil else { return self }
        self.method = method
        request.httpMethod = method.rawValue
        return self
    }
    
    func parameters(_ params: [String:Any]) -> Self {
        guard request != nil else { return self }

        var resultQuery: String = ""
        for (key, value) in params {
            if !resultQuery.isEmpty {
                resultQuery += "&"
            }
            if let value = value as? String {
                resultQuery += key + "=" + value
            } else if let value = value as? Int {
                resultQuery += key + "=" + "\(value)"
            } else if let paramArray = value as? [String] {
                for value in paramArray {
                    resultQuery += key + "=" + value
                }
            }
        }
        guard let query = resultQuery.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            return self
        }
        if method == .get {
            let urlString = request.url!.absoluteString + "?" + query
            request = URLRequest(url: URL(string: urlString)!)
            request.httpMethod = DKNetworkRequestMethod.get.rawValue
        } else {
            request.httpBody = query.data(using: .utf8)
        }
        return self
    }
    
    func headers(_ headers: [String:String]) -> Self {
        guard request != nil else { return self }
        for (key, value) in headers {
            request.addValue(value, forHTTPHeaderField: key)
        }
        return self
    }
    
    func send(_ completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        guard request != nil else { return }
        print("Request - \(method.rawValue): \(request.url!.absoluteString)")
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(data, response, error)
                self.request = nil
                self.method = .get
                guard let data = data, let response = response as? HTTPURLResponse else { return }
                if let content = String(data: data, encoding: .utf8) {
                    print(String(format: "Response %ld: %@", response.statusCode, content))
                }

            }
        }.resume()
    }
    
}

extension DKNetwork {
    func sendAuth(_ completion: @escaping (TokenModel?) -> Void) {
        DKNetwork.shared.send { (data, response, error) in
            guard let data = data, error == nil else { completion(nil); return }
            if let result = try? JSONDecoder().decode(AuthResult.self, from: data) {
                if let tokenModel = result.result, (result.code == 200 || result.code == 201) {
                    completion(tokenModel)
                } else {
                    completion(nil)
                }
            } else {
                completion(nil)
            }
        }
    }
    func sendUser(_ completion: @escaping (UserResult?)->Void) {
        DKNetwork.shared.send { (data, response, error) in
            guard let data = data, error == nil else { completion(nil); return }
            if let result = try? JSONDecoder().decode(UserResult.self, from: data) {
                completion(result)
            } else {
                completion(nil)
            }
        }
    }
    func sendProduct(_ completion: @escaping ([ProductModel])->Void) {
        DKNetwork.shared.send { (data, response, error) in
            guard let data = data, error == nil else { completion([]); return }
            print(String(data: data, encoding: .utf8)!)
            if let result = try? JSONDecoder().decode(ProductResult.self, from: data), let results = result.results {
                completion(results)
            } else {
                completion([])
            }
        }
    }
}
