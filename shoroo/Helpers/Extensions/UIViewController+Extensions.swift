//
//  UIViewController+Extensions.swift
//  shoroo
//
//  Created by kuzindmitry on 16.02.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import UIKit

extension UIViewController {
    
    @IBAction func backToPreviousViewController(_ sender: Any) {
        popViewController()
    }
    
    func popViewController() {
        let _ = navigationController?.popViewController(animated: true)
    }
    
}
