//
//  BannerPresentable.swift
//  
//
//  Created by kuzindmitry on 05.02.2018.
//

import UIKit
import BRYXBanner

let maxBannerTextLength: Int = 90
let BannerDurationOnScreen: TimeInterval = 3.0

protocol BannerPresentable {
    var allowedToShowBanner: Bool { get set }
    func showBanner(title: String?, subtitle: String?, color: UIColor, alpha: CGFloat, didTapBlock:(()->Void)?)
}

extension BannerPresentable where Self: UIViewController {
    
    func showBanner(title: String? = nil, subtitle: String? = nil, color: UIColor = UIColor.brandActiveColor(), alpha: CGFloat = 0.95, didTapBlock:(()->Void)? = nil) {
        if allowedToShowBanner == false {
            return
        }
        let stitle = subtitle?.substring(maxLength: maxBannerTextLength)
        let banner = Banner(title: title, subtitle: stitle, backgroundColor: color, didTapBlock: didTapBlock)
        banner.adjustsStatusBarStyle = true
        banner.hasShadows = false
        banner.alpha = alpha
        
        banner.show(duration: BannerDurationOnScreen)
    }
    
}
