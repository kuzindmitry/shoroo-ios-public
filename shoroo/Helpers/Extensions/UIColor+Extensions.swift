//
//  UIColor+Extensions.swift
//  shoroo
//
//  Created by kuzindmitry on 16.02.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import UIKit


extension UIColor {
    
    class func brandYellow() -> UIColor {
        return try! UIColor(rgba_throws: "#FFCC00")
    }
    
    class func brandActiveColor() -> UIColor {
        return try! UIColor(rgba_throws: "#47B3C3")
    }
    
    class func brandTurquoise() -> UIColor {
        return try! UIColor(rgba_throws: "#58C9DA")
    }
    
    class func brandLightGray() -> UIColor {
        return try! UIColor(rgba_throws: "#EFEFF4")
    }
    
    class func brandGray() -> UIColor {
        return try! UIColor(rgba_throws: "#999FAB")
    }
    
    class func brandDarkGray() -> UIColor {
        return try! UIColor(rgba_throws: "#596168")
    }
    
    class func brandBlue() -> UIColor {
        return try! UIColor(rgba_throws: "#5e98e7")
    }
    
    class func brandRed() -> UIColor {
        return try! UIColor(rgba_throws: "#C32B2B")
    }
    
    class func incomingBubbleColor() -> UIColor {
        return UIColor.white
    }
    
    class func outgoingBubbleColor() -> UIColor {
        return try! UIColor(rgba_throws: "#E0E1E9")
    }
    
    class func chatSelectedMessageColor() -> UIColor {
        return try! UIColor(rgba_throws: "#4797C3").withAlphaComponent(0.1)
    }
    
}

class AuthParameters {
    let email: String
    let password: String
    let username: String?
    
    init(email: String, password: String, username: String? = nil) {
        self.email = email
        self.password = password
        self.username = username
    }
    
    func convert() -> [String: String] {
        var result: [String: String] = [:]
        result["email"] = email
        result["password"] = password
        if let username = username {
            result["username"] = username
        }
        return result
    }
}

extension String {
    
    func height(with font: UIFont?, width: CGFloat) -> CGFloat {
        guard let font = font else { return 0 }
        let size = CGSize(width: width, height: CGFloat(MAXFLOAT))
        let label = NSString(string: self).boundingRect(with: size, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedStringKey.font: font], context: nil)
        return label.size.height
    }
    
    func substring(maxLength: Int) -> String {
        return self.count > maxLength ? self[Range(self.startIndex..<self.index(self.startIndex, offsetBy: maxLength-3))] + "..." : self
    }
    
    var trimmedWhitespaces: String {
        return trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func initials() -> String {
        var fn = ""
        var ln: String?
        var initials = ""
        
        let components = self.components(separatedBy: " ")
        
        if components.count >= 1 {
            if let firstName = components.first {
                fn = firstName
            }
            
            if components.count == 2 {
                let lastName = components[1]
                ln = lastName
            }
        }
        
        if let first = fn.first {
            initials = String(first)
        }
        if let lastName = ln {
            if let second = lastName.first {
                initials.append(second)
            }
        }
        
        return initials.uppercased()
    }
    
}

public enum UIColorInputError : Error {
    case missingHashMarkAsPrefix,
    unableToScanHexValue,
    mismatchedHexStringLength
}

extension UIColor {
    
    public convenience init(rgba_throws rgba: String) throws {
        guard rgba.hasPrefix("#") else {
            throw UIColorInputError.missingHashMarkAsPrefix
        }
        
        let hexString: String = rgba.substring(from: rgba.characters.index(rgba.startIndex, offsetBy: 1))
        var hexValue:  UInt32 = 0
        
        guard Scanner(string: hexString).scanHexInt32(&hexValue) else {
            throw UIColorInputError.unableToScanHexValue
        }
        
        let divisor = CGFloat(255)
        let red     = CGFloat((hexValue & 0xFF0000) >> 16) / divisor
        let green   = CGFloat((hexValue & 0x00FF00) >>  8) / divisor
        let blue    = CGFloat( hexValue & 0x0000FF       ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
    public func hexString(_ includeAlpha: Bool = true) -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        if includeAlpha {
            return String(format: "#%02X%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255), Int(a * 255))
        } else {
            return String(format: "#%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255))
        }
    }
    
}

extension String {
    
    public func argb2rgba() -> String? {
        guard self.hasPrefix("#") else {
            return nil
        }
        
        let hexString: String = self.substring(from: self.characters.index(self.startIndex, offsetBy: 1))
        switch (hexString.characters.count) {
        case 4:
            return "#"
                + hexString.substring(from: self.characters.index(self.startIndex, offsetBy: 1))
                + hexString.substring(to: self.characters.index(self.startIndex, offsetBy: 1))
        case 8:
            return "#"
                + hexString.substring(from: self.characters.index(self.startIndex, offsetBy: 2))
                + hexString.substring(to: self.characters.index(self.startIndex, offsetBy: 2))
        default:
            return nil
        }
    }
    
}
