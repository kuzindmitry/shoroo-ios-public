//
//  SHButton.swift
//  shoroo
//
//  Created by kuzindmitry on 15.02.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import UIKit

@IBDesignable
class SHButton: UIButton {
    
    let shadowOpacity: Float = 0.1
    let shadowRadius: CGFloat = 20.0
    let shadowColor: UIColor = .black
    @IBInspectable var buttonColor: UIColor = .white
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 6)
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = shadowOpacity
        clipsToBounds = false
        if imageView != nil {
            imageView?.frame.size = CGSize(width: frame.height, height: frame.height)
            imageView?.contentMode = .scaleAspectFit
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
         layer.cornerRadius = frame.size.height/2
    }
    
    
    @IBInspectable var activityIndicatorTintColor: UIColor = .white
    private var activity: UIActivityIndicatorView!
    private var lastTitle: String?
    
    func startProgress() {
        guard activity == nil else { return }
        activity = UIActivityIndicatorView(activityIndicatorStyle: .white)
        activity.color = activityIndicatorTintColor
        activity.hidesWhenStopped = true
        activity.startAnimating()
        activity.frame = bounds
        lastTitle = titleLabel?.text
        setTitle(nil, for: .normal)
        addSubview(activity)
    }
    
    func stopProgress() {
        guard activity != nil else { return }
        activity.stopAnimating()
        activity.removeFromSuperview()
        activity = nil
        setTitle(lastTitle, for: .normal)
        lastTitle = nil
    }
    
    override var isEnabled: Bool {
        didSet {
            if isEnabled {
                titleLabel?.alpha = 1
                backgroundColor = buttonColor
            } else {
                titleLabel?.alpha = 0.3
                backgroundColor = buttonColor.withAlphaComponent(0.3)
            }
        }
    }
    
}
