//
//  PTextField.swift
//  pchat
//
//  Created by kuzindmitry on 05.02.2018.
//  Copyright © 2018 TASS. All rights reserved.
//

import UIKit

@IBDesignable
class SHTextField: UITextField {
    @IBInspectable var insetX: CGFloat = 0
    @IBInspectable var insetY: CGFloat = 0
    @IBInspectable var activeBorderColor: UIColor = .yellow
    @IBInspectable var isPresentFlag: Bool = false {
        didSet {
            if isPresentFlag {
                layer.borderWidth = 1
                layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
            }
        }
    }
    @IBInspectable var isFlagValid = false {
        didSet {
            if isFlagValid {
                layer.borderColor = activeBorderColor.cgColor
            } else {
                layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = frame.size.height/2
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
}
