//
//  ProfileViewController.swift
//  shoroo
//
//  Created by kuzindmitry on 19.02.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import UIKit

class ProfileViewController: UITableViewController {
    
    @IBOutlet weak var initialsLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user = DataManager.shared.currentUser() {
            nameLabel.text = user.firstName + "\n" + user.lastName
            initialsLabel.text = nameLabel.text?.initials()
        } else {
            UIApplication.shared.keyWindow?.rootViewController = R.storyboard.authorization.instantiateInitialViewController()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if case(0, 1) = (indexPath.section, indexPath.row) {
            Database.shared.removeBase()
            UIApplication.shared.keyWindow?.rootViewController = R.storyboard.authorization.instantiateInitialViewController()
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
