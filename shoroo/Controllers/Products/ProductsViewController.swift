//
//  ViewController.swift
//  shoroo
//
//  Created by kuzindmitry on 01.01.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import UIKit
import YYWebImage
import CollectionKit

class ProductsViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var products: [Product] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        update()
    }

    func update(offset: Int = 0) {
        APIManager.shared.products(offset: offset) { (newProducts) in
            self.products += newProducts
            self.collectionView.reloadData()
        }
    }
    
}

extension ProductsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.productCollectionViewCell, for: indexPath)!
        cell.imageView.yy_setImage(with: products[indexPath.row].imageUrl, options: .setImageWithFadeAnimation)
        cell.titleLabel.text = products[indexPath.row].title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (UIScreen.main.bounds.width - 60.0) / 2
        let height = width * 1.5
        return CGSize(width: width, height: height)
    }
    
}
