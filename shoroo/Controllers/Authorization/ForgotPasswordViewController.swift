//
//  ForgotPasswordViewController.swift
//  shoroo
//
//  Created by kuzindmitry on 19.02.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController, BannerPresentable {
    
    var allowedToShowBanner: Bool = true
    @IBOutlet weak var emailField: SHTextField!
    @IBOutlet weak var resetButton: SHButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        resetButton.isEnabled = false
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let event = event, event.type == .touches {
            view.endEditing(true)
        }
    }
    
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        let isEmailValid = emailValid(emailField.text)
        resetButton.isEnabled = isEmailValid
    }
    
    func emailValid(_ email: String?) -> Bool {
        guard let email = email, !email.isEmpty else { return false }
        let isValid = email.range(of: "@") != nil && email.range(of: ".") != nil
        emailField.isFlagValid = isValid
        return isValid
    }
    
    @IBAction func resetButtonDidTap(_ sender: UIButton) {
        reset()
    }
    
    func reset() {
        guard let email = emailField.text else { return }
        guard emailValid(email) else { return }
        resetButton.startProgress()
        APIManager.shared.resetPassword(email: email) { (isSuccessed) in
            self.resetButton.stopProgress()
            self.resetButton.isEnabled = false
            if isSuccessed {
                self.showBanner(title: "Success", subtitle: "A password reset request has been generated. Go to the mail.", color: UIColor.brandTurquoise())
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                    self.popViewController()
                })
            } else {
                self.showBanner(title: "Error", subtitle: "Incorrect user data. Try again later.", color: UIColor.brandRed())
            }
        }
    }
    
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailField {
            emailField.resignFirstResponder()
            if resetButton.isEnabled {
                reset()
            }
        }
        return true
    }
    
}

