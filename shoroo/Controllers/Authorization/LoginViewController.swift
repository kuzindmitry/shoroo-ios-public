//
//  LoginViewController.swift
//  shoroo
//
//  Created by kuzindmitry on 19.02.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, BannerPresentable {
    
    var allowedToShowBanner: Bool = true
    @IBOutlet weak var emailField: SHTextField!
    @IBOutlet weak var passwordField: SHTextField!
    @IBOutlet weak var loginButton: SHButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        loginButton.isEnabled = false
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let event = event, event.type == .touches {
            view.endEditing(true)
        }
    }
    
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        let isEmailValid = emailValid(emailField.text)
        let isPasswordValid = passwordValid(passwordField.text)
        loginButton.isEnabled = isEmailValid && isPasswordValid
    }
    
    func emailValid(_ email: String?) -> Bool {
        guard let email = email, !email.isEmpty else { return false }
        let isValid = email.range(of: "@") != nil && email.range(of: ".") != nil
        emailField.isFlagValid = isValid
        return isValid
    }
    
    func passwordValid(_ password: String?) -> Bool {
        guard let password = password, !password.isEmpty else { return false }
        let isValid = password.count >= 8
        passwordField.isFlagValid = isValid
        return isValid
    }
    
    @IBAction func loginButtonDidTap(_ sender: UIButton) {
        login()
    }
    
    func login() {
        guard let email = emailField.text, let password = passwordField.text else { return }
        guard emailValid(email) && passwordValid(password) else {
            return
        }
        loginButton.startProgress()
        APIManager.shared.signin(email: email, password: password) { (token) in
            self.loginButton.stopProgress()
            guard token != nil else {
                self.showBanner(title: "Error", subtitle: "Incorrect user data. Try again later.", color: UIColor.brandRed())
                return
            }
            let mainViewController = R.storyboard.main.instantiateInitialViewController()!
            self.present(mainViewController, animated: true, completion: nil)
        }
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailField {
            passwordField.becomeFirstResponder()
        } else if textField == passwordField {
            passwordField.resignFirstResponder()
            if loginButton.isEnabled {
                login()
            }
        }
        return true
    }
    
}

