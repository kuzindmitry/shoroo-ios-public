//
//  SocialLoginViewController.swift
//  shoroo
//
//  Created by kuzindmitry on 19.02.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import UIKit
import WebKit

class SocialLoginViewController: UIViewController, BannerPresentable {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    var type: SocialType = .vk
    var allowedToShowBanner: Bool = true
    var webView: WKWebView!
    var activityIndicatorView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = type.rawValue.uppercased()
        
        navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        
        let configuration = WKWebViewConfiguration()
        webView = WKWebView(frame: containerView.bounds, configuration: configuration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        containerView.addSubview(webView)
        
        removePreviousData()
        
        activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicatorView.frame = view.bounds
        activityIndicatorView.hidesWhenStopped = true
        view.addSubview(activityIndicatorView)
     
        webView.load(URLRequest(url: URL(string: "https://oauth.vk.com/authorize?client_id=6373750&display=mobile&redirect_uri=https://oauth.vk.com/blank.html&scope=friends,email,offline&revoke=1&response_type=token&v=5.73&state=123456")!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 60.0))
        activityIndicatorView.startAnimating()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        webView.frame = containerView.bounds
    }
    
    func removePreviousData() {
        let websiteDataTypes = Set([WKWebsiteDataTypeDiskCache, WKWebsiteDataTypeMemoryCache, WKWebsiteDataTypeSessionStorage, WKWebsiteDataTypeCookies])
        WKWebsiteDataStore.default().removeData(ofTypes: websiteDataTypes, modifiedSince: Date(timeIntervalSince1970: 0), completionHandler: {})
    }
    
}

extension SocialLoginViewController: WKUIDelegate, WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if activityIndicatorView.isAnimating {
            activityIndicatorView.stopAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        guard let urlString = webView.url?.absoluteString else { return }
        
        guard urlString.range(of: "#") != nil else { return }
        if let query = urlString.components(separatedBy: "#").last {
            webView.isHidden = true
            if query.range(of: "access_token") != nil {
                var result = VKLoginResult()
                let isParsed = result.parse(query)
                if isParsed {
                    if !activityIndicatorView.isAnimating {
                        activityIndicatorView.startAnimating()
                    }
                    APIManager.shared.socialAuth(type: .vk, email: result.email, token: result.accessToken, userId: result.user_id, { (token) in
                        guard token != nil else {
                            self.showBanner(title: "Error", subtitle: "Incorrect user data. Try again later.", color: UIColor.brandRed())
                            return
                        }
                        let mainViewController = R.storyboard.main.instantiateInitialViewController()!
                        self.present(mainViewController, animated: true, completion: nil)
                    })
                } else {
                    showBanner(title: "Error", subtitle: "Incorrect user data. Try again later.", color: UIColor.brandRed())
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                        self.popViewController()
                    })
                }
            } else if query.range(of: "error") != nil {
                showBanner(title: "Error", subtitle: "Incorrect user data. Try again later.", color: UIColor.brandRed())
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                    self.popViewController()
                })
            }
        }
       
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        if !activityIndicatorView.isAnimating {
            activityIndicatorView.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
    }
}

struct VKLoginResult {
    
    var accessToken: String = ""
    var email: String = ""
    var user_id: String = ""
    
    mutating func parse(_ query: String) -> Bool {
        let parameters = query.components(separatedBy: "&")
        var result = [String:String]()
        for parameter in parameters {
            let res = parameter.components(separatedBy: "=")
            if res.count == 2 {
                result[res.first!] = res.last!
            }
        }
        if let token = result["access_token"], let email = result["email"], let userId = result["user_id"] {
            accessToken = token.trimmingCharacters(in: .whitespacesAndNewlines)
            self.email = email.trimmingCharacters(in: .whitespacesAndNewlines)
            user_id = userId.trimmingCharacters(in: .whitespacesAndNewlines)
            return true
        } else {
            accessToken = ""
            email = ""
            user_id = ""
            return false
        }
    }
    
}
