//
//  RegistrationViewController.swift
//  shoroo
//
//  Created by kuzindmitry on 16.02.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController, BannerPresentable {
    
    var allowedToShowBanner: Bool = true
    @IBOutlet weak var emailField: SHTextField!
    @IBOutlet weak var passwordField: SHTextField!
    @IBOutlet weak var nameField: SHTextField!
    @IBOutlet weak var surnameField: SHTextField!
    @IBOutlet weak var registerButton: SHButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        registerButton.isEnabled = false
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let event = event, event.type == .touches {
            view.endEditing(true)
        }
    }
    
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        let isNamesValid = namesValid(name: nameField.text, surname: surnameField.text)
        let isEmailValid = emailValid(emailField.text)
        let isPasswordValid = passwordValid(passwordField.text)
        registerButton.isEnabled = isNamesValid && isEmailValid && isPasswordValid
    }
    
    func emailValid(_ email: String?) -> Bool {
        guard let email = email, !email.isEmpty else { return false }
        let isValid = email.range(of: "@") != nil && email.range(of: ".") != nil
        emailField.isFlagValid = isValid
        return isValid
    }
    
    func passwordValid(_ password: String?) -> Bool {
        guard let password = password, !password.isEmpty else { return false }
        let isValid = password.count >= 8
        passwordField.isFlagValid = isValid
        return isValid
    }
    
    func namesValid(name: String?, surname: String?) -> Bool {
        guard let name = name, let surname = surname else { return false }
        nameField.isFlagValid = !name.isEmpty
        surnameField.isFlagValid = !surname.isEmpty
        return !name.isEmpty && !surname.isEmpty
    }
    
    @IBAction func registerButtonDidTap(_ sender: UIButton) {
        register()
    }
    
    func register() {
        guard let email = emailField.text, let password = passwordField.text, let name = nameField.text, let surname = surnameField.text else { return }
        guard emailValid(email) && passwordValid(password) && namesValid(name: name, surname: surname) else {
            return
        }
        registerButton.startProgress()
        APIManager.shared.signup(email: email, password: password, firstName: name, lastName: surname) { (token) in
            self.registerButton.stopProgress()
            guard token != nil else {
                self.showBanner(title: "Error", subtitle: "Incorrect user data. Try again later.", color: UIColor.brandRed())
                return
            }
            let mainViewController = R.storyboard.main.instantiateInitialViewController()!
            self.present(mainViewController, animated: true, completion: nil)
        }
        
    }
}

extension SignUpViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameField {
            surnameField.becomeFirstResponder()
        } else if textField == surnameField {
            emailField.becomeFirstResponder()
        } else if textField == emailField {
            passwordField.becomeFirstResponder()
        } else if textField == passwordField {
            passwordField.resignFirstResponder()
            if registerButton.isEnabled {
                register()
            }
        }
        return true
    }
    
}
