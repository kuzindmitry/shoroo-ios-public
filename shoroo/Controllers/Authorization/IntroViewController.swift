//
//  FirstViewController.swift
//  shoroo
//
//  Created by kuzindmitry on 15.02.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController {
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = true
        if UIScreen.main.bounds.height <= 568.0 {
            topConstraint.constant = 24.0
        }
        bottomConstraint.constant = UIScreen.main.bounds.width/10
    }
    
}
