//
//  AppDelegate.swift
//  shoroo
//
//  Created by kuzindmitry on 01.01.2018.
//  Copyright © 2018 kuzindmitry. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if DataManager.shared.tokensCount() == 0 {
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = R.storyboard.authorization.instantiateInitialViewController()!
            window?.makeKeyAndVisible()
        }
        
        
        return true
    }
    
}

